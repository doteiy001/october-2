#include <iostream>
#include <conio.h>
#include <list>
using namespace std;

class Node {
public:
		string	Title;
		string	Artist;
		int Index;
		Node*	next;
};
class List{
	public:
			List(void)	{head = NULL;	}
			~List(void);
			
			bool isEmpty(){ return head == NULL;	}
			Node* Add(int index,string Title, string Artist);
			int Find(string x);
			int Delete(int index);
			void DisplayList(void);
			void Edit(int index, string newTitle, string Artist);
			string ViewMusic(int index);
			int Size(Node* index);
	private:
			Node* head;
			Node* artist;
};
int List::Find(string x){
	Node* CurrNode	=	head;
	Node* CurrArtist =	artist;
	int	CurrIndex	=	1;
	while (CurrNode && CurrNode->Title != x){
		CurrNode	=	CurrNode->next;
		CurrIndex++;
	}
	if (CurrNode) return CurrIndex;
	return 0;
}

int List::Size(Node* index){
    int count = 0; // Initialize count  
    Node* current = head; // Initialize current  
    while (current != NULL)  
    {  
        count++;  
        current = current->next;  
    }  
    return count;  
}  

int List:: Delete(int index){
	Node* prevNode	=	NULL;
	Node* CurrNode	=	head;
	Node* prevArtist = NULL;
	Node* CurrArtist = artist;
	int CurrIndex	=	1;
	while (CurrIndex != index){
		prevNode	=	CurrNode;
		CurrNode	=	CurrNode->next;
		prevArtist = CurrArtist;
		CurrArtist = CurrArtist->next;
		CurrIndex++;
	}
	if (CurrNode){
		if(prevNode && prevArtist){
			prevNode->next	=	CurrNode->next;
			prevArtist->next = CurrArtist->next;
			delete CurrNode;
		}
		else{
			head	=	CurrNode->next;
			artist = CurrArtist->next;
			delete	CurrNode;
			delete	CurrArtist;
		}
		return	CurrIndex;
	}
	return 0;
}
string List::ViewMusic(int index){
	Node* CurrNode	=	head;
	int	CurrIndex	=	1;
	while (CurrIndex != index){
		CurrNode	=	CurrNode->next;
		CurrIndex++;
	}
	if (CurrNode) return CurrNode->Title;
	//return NULL;
}

void List::DisplayList(){
	int num = 0;
	Node* CurrNode	=	head;
	Node* CurrArtist = 	artist;
	while (CurrNode != NULL &&CurrArtist != NULL){
		cout<<"["<<num+1<<"]"<<CurrNode->Title<<" ";
		cout<<" "<<"by "<<" "<<CurrArtist->Artist<<endl;;
		CurrArtist = CurrArtist->next;
		CurrNode	=	CurrNode->next;
		num++;
	}
}

Node* List::Add(int index,string Title, string Artist){
	if (index < 0) return NULL;
	int CurrIndex = 1;
	Node* CurrNode =head;
	Node* CurrArtist = artist;
	while (CurrNode && index > CurrIndex){
		CurrNode	=	CurrNode->next;
		CurrIndex++;
		CurrArtist	=	CurrArtist->next;
	}
	Node* newNode =		new		Node;
	newNode->Title = 	Title;
	Node* newArtist = new Node;
	newArtist->Artist = Artist;
	if (index==0){
			newNode->next	=	head;
			head 			=	newNode;
			newArtist->next = artist;
			artist = newArtist;
	}
	else{
		newNode->next	=	CurrNode->next;
		CurrNode->next	=	newNode;
		newArtist->next = CurrArtist->next;
		CurrArtist->next = newArtist;
	}
	return newNode;
}


List::~List(void){
	Node* CurrNode	=	head,	*nextNode	=	NULL;
	while (CurrNode != NULL){
		nextNode	=	CurrNode->next;
		delete	CurrNode;
		CurrNode	=	nextNode;
	}
}

List Music;

int switchHandler(int choice){
	Node* CurrNode;
	string Title,Artist,editName,searchName;
	int delMusic,editNumber,playNumber,musicSize;
	//cout<<"The Playlist Currently has "<<Music.Size(CurrNode)<<" elements.\n";
	system("CLS");
	cout<<"1.) Add a Music\n2.) Delete a Music\n3.) Edit a Music Title\n4.) Play a Music\n5.) View Playlist\n";
	cout<<"What would you like to do? > ";
	cin>>choice;
	if(cin.fail()){
			cout<<"Invalid Input.\n";
			cin.clear();
			cin.ignore();
			system("pause");
		}
	else{
		switch(choice){
			case 1://Add
				cout<<"Enter Title of the Music to be added > ";
				cin.ignore();
				getline(cin,Title);
				cout<<"Enter Artist > ";
				getline(cin,Artist);
				Music.Add(0,Title,Artist);
				cout<<"Current Playlist : \n";
				Music.DisplayList();
			break;
			case 2://Delete
				Music.DisplayList();
				cout<<"Enter Number of Music you want to be deleted > ";
				cin>>delMusic;
				if(cin.fail()){
					cout<<"Invalid Input.\n";
					cin.clear();
					cin.ignore();
					system("Pause");
				}else{
				Music.Delete(delMusic);
				}
				Music.DisplayList();
			break;
			case 3://Edit
				Music.DisplayList();
				cout<<"Choose the number of the music title you want to edit : ";
				cin>>editNumber;
				if(cin.fail()){
					cout<<"Invalid Input.\n";
					cin.clear();
					cin.ignore();
					system("Pause");
				}
				else{
				Music.Delete(editNumber);
				cout<<"Enter the new music title : ";
				cin.ignore();
				getline(cin,editName);
				cout<<"Enter Name of New Artist > ";
				getline(cin,Artist);
				}
				cout<<endl;
				Music.Add(editNumber-1,editName, Artist);
				Music.DisplayList();
			break;
			case 4://Play
				Music.DisplayList();
				cout<<"Choose the number of the music you want to play >  ";
				cin>>playNumber;
				if(cin.fail()){
					cout<<"Invalid Input.\n";
					cin.clear();
					cin.ignore();
					system("Pause");
				}
				else{
					//Previous
					if(playNumber-1 <= 0)	{
						cout<<"No Song Available.\n";
					}else{
						cout<<"Previous : "<<Music.ViewMusic(playNumber-1)<<endl;
					}
					//Current
					if(playNumber >Music.Size(CurrNode)){
						cout<<"No Song Available.\n";
					}
					else{
						cout<<"Now Playing : "<<Music.ViewMusic(playNumber)<<endl;
					}
					//Next
					if(playNumber+1 > Music.Size(CurrNode)){
						cout<<"No More Songs to Play.\n";
					}
					else{
					cout<<"Next : "<<Music.ViewMusic(playNumber+1)<<endl;	
					}
				}
			break;
			case 5://view
				cout<<"Current Playlist is : "<<endl;
				Music.DisplayList();
			break;
			default:
				cout<<"No such option.\n";
			break;
		}
	}
}

int main(){
	Node* CurrNode;
	int choice;
	char yesSwitch = 'y';
	Music.Add(0,"Mundo","IV of Spades");
	Music.Add(1,"Buwan","Juan Karlos Labajo");
	Music.Add(2,"Be My Mistake","The 1975");
	Music.Add(3,"Sayo","FourPlay MNL");
	Music.Add(4,"Killer in the Mirror", "Set it Off");
	while((yesSwitch == 'Y')||(yesSwitch == 'y')){
		system("CLS");
		switchHandler(choice);
		cout<<"Would you like to stay?(y/n) > ";
		cin>>yesSwitch;
	}
	cout<<"Come Again!\n";
}